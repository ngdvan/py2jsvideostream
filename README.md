Stream Video from Python (with opencv feed) to Nodejs using mqtt and socket.io

The project uses opencv to capture usb webcam camrea feed, encode each frame to base64 and send it over the network using the public mosquito mqtt test broker.
An expressjs webserver is setup to receive the frame. A websocket (using socketio) is then streaming frame to the front-end


