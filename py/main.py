import cv2
import paho.mqtt.client as mqtt
import base64
import time

client = mqtt.Client()
client.connect("test.mosquitto.org")

cap = cv2.VideoCapture(0)

while True:
    start = time.time()
    ret, frame = cap.read()
    if not ret:
        break
    if frame is None:
        break
    _, buffer = cv2.imencode('.jpg', frame)
    b64 = base64.b64encode(buffer)
    client.publish("vpy/frame", b64,qos=0, retain=False)
    end = time.time()
    t = end-start
    fps = 1/t
    print("FPS: {}".format(fps))
    
    
    cv2.imshow('live', frame)
    key = cv2.waitKey(100)
    if key & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break
